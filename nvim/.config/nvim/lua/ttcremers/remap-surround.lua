-- Handy but also here for documentation purposes.

-- Surround a _selection_ by given character
-- Note: This won't work with cap V selections. Challenge
vim.keymap('v',"<leader>S(", "c()<Esc>P")
vim.keymap('v','<leader>S"', 'c""<Esc>P')
vim.keymap('v',"<leader>S'", "c''<Esc>P")
vim.keymap('v',"<leader>S{", "c{}<Esc>P")

-- Surround a _word_ by given character
vim.keymap('n',"<leader>sw(", "ciw()<Esc>P")
vim.keymap('n','<leader>sw"', 'ciw""<Esc>P')
vim.keymap('n',"<leader>sw'", "ciw''<Esc>P")
vim.keymap('n',"<leader>sw{", "ciw{}<Esc>P")

-- Surround a _word_ by given character
vim.keymap('n',"<leader>sW(", "ciW()<Esc>P")
vim.keymap('n','<leader>sW"', 'ciW""<Esc>P')
vim.keymap('n',"<leader>sW'", "ciW''<Esc>P")
vim.keymap('n',"<leader>sW{", "ciW{}<Esc>P")
