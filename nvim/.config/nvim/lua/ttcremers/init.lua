require("ttcremers/set")
require("ttcremers/packer")
require("ttcremers/remap")

local autocmd = vim.api.nvim_create_autocmd
local augroup = vim.api.nvim_create_augroup

vim.g.netrw_browse_split = 0
vim.g.netrw_banner = 1
vim.g.netrw_winsize = 25

-- My private group
TtcremersGroup = augroup('Ttcremers', {})

-- Remove trailing white spaces on save
autocmd({"BufWritePre"}, {
    group = TtcremersGroup,
    pattern = "*",
    command = "%s/\\s\\+$//e",
})

-- Reload Kitty Term config after saving config file
autocmd({"BufWritePre"}, {
    group = TtcremersGroup,
    pattern = "kitty.conf",
    command = "!(kill -SIGUSR1 `pidof kitty` && notify-send Kitty purrrrrr)",
})

-- Configure Vimwiki here as this needs to be set-up before the plugin is loaded
vim.g.vimwiki_global_ext = 0
vim.g.vimwiki_list = {{path = '~/vimwiki/main/wiki', syntax = 'markdown', ext = '.md'}}
