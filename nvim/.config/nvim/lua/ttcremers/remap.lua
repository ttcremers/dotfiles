-- Toggle NetRW
vim.keymap.set('n',"<C-f>", "<cmd>Ex<CR>")

-- Helper to get a file relative to our config directory.
-- NOTE I'm guessing there's a var somewhre that holds to config
--      directory path. Couldn't find it, didn't want to search.
function getFileFromCfgFn(file)
  return function ()
    vim.cmd.vsplit(vim.fn.fnamemodify(os.getenv("MYVIMRC"), ':p:h') .. file)
  end
end
-- Edit vim config in vertical split
vim.keymap.set('n',"<leader>vv", "<cmd>vsplit $MYVIMRC<cr>")
-- Edit the keymap file directly. I'm forget full
vim.keymap.set('n',"<leader>vk", getFileFromCfgFn('/lua/ttcremers/remap.lua'))
-- Edit the LSP config file directly. I'm forget full
vim.keymap.set('n',"<leader>vk", getFileFromCfgFn('/after/plugin/lsp.lua'))

-- Window Vertical Split
vim.keymap.set('n',"<leader>vs", "<cmd>vsplit<cr>")
vim.keymap.set('n',"<leader>hs", "<cmd>hsplit<cr>")

-- Buffer <leader> commands
vim.keymap.set('n',"<leader>bd", "<cmd>bdelete<cr>")

-- Git Neogit
vim.keymap.set('n',"<leader>gg", "<cmd>Neogit")

-- Half page jumps with view port centering
vim.keymap.set('n',"<C-d>", "<C-d>zz")
vim.keymap.set('n',"<C-u>", "<C-u>zz")
--
-- Search jumps with view port centering
vim.keymap.set('n',"n", "nzzzv")
vim.keymap.set('n',"N", "Nzzzv")

-- Don't move the cursor when using J to append lines to each other
-- [m]ark current position to 'z' execute J [`] jump back to 'z'
vim.keymap.set('n', "J", "mzJ`z")

-- Move selection taking indentation into account
vim.keymap.set('v', "J", ":move '>+1<CR>gv=gv")
vim.keymap.set('v', "K", ":move '<-2<CR>gv=gv")

-- Extra Ooohm
-- Paste over a selection _without_ overwriting your past clipboard with the
-- text you effectively deleted (only when using <leader>p of course)
vim.keymap.set('x', "<leader>p", "\"_dP")

-- In the current buffer replace the word under the cursor.
vim.keymap.set('n', "<leader>wr", ":%s/\\<<C-r><C-w>\\>/<C-r><C-w>/gI<Left><Left><Left>")
-- In the current buffer replace the active selection. 'sno' makes this literal
vim.keymap.set('v', "<leader>yr", "y:sno/<C-r>\"/<C-r>\"/gI<Left><Left><Left>")

-- COPY & PASTE
--   So awesome it deserves its own file with explanation
--
-- Make ctrl-c/ctrl-v/ctrl-x work as you expect on x windowing
-- system.
vim.keymap.set('i',"<C-v>", "<ESC>\"+pa")
vim.keymap.set('v',"<C-c>", "\"+y")
vim.keymap.set('v',"<C-x>", "\"+d")

--[[ Better split navigation
-- Maybe sticking to the default isn't that bad
vim.keymap.set('n',"<leader>l <C-w><C-l>")
vim.keymap.set('n',"<leader>j <C-w><C-j>")
vim.keymap.set('n',"<leader>k <C-w><C-k>")
vim.keymap.set('n',"<leader>h <C-w><C-h>")
]]

