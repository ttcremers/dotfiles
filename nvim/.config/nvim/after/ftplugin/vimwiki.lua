vim.opt.textwidth = 80
vim.opt.wrap = true
vim.opt.linebreak = true
vim.api.nvim_command('set formatoptions +=t')
