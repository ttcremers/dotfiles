local builtin = require('telescope.builtin')

-- Don't know yet if this is really usable. Wouldn't this mean you would always
-- have to git add files as soon as you create them?
vim.keymap.set('n', '<leader><leader>', builtin.git_files, {})

vim.keymap.set('n', '<leader>ff', builtin.find_files, {})
vim.keymap.set('n', '<leader>fc', builtin.commands, {})

vim.keymap.set('n', '<leader>fb', builtin.buffers, {})
vim.keymap.set('n', '<leader>fh', builtin.help_tags, {})

vim.keymap.set('n', '<leader>gr', builtin.live_grep, {})
