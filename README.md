# GNU Stow managed dotfiles

Pick the dot files you want to use and stow them. Requires the gnu stow package.

Undocumented in `stow --help` this command would stow the `bash/dot-bashrc` as `.bashrc` in your home directory. Sadly this seems to only work for files and not for directories.

```
$ stow --dotfiles bash
```

